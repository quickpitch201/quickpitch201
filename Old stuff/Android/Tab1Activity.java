package edu.usc.emanuel_azage.custumtab;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Tab1Activity extends AppCompatActivity {

    private Button mTab1Button;
    private Button mTab2Button;
    private Button mTab3Button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab1);

        //if btn1 is pressed, refresh
        //if btn2 is pressed, go to tab2
        //if btn3 is pressed, go to tab3

        mTab1Button = (Button)findViewById(R.id.tab1Button);
        mTab2Button = (Button)findViewById(R.id.tab2Button);
        mTab3Button = (Button)findViewById(R.id.tab3Button);

        final Context Tab1Activity = this;
        mTab2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tab1Activity, Tab2Activity.class);
                startActivity(intent);
            }
        });

        mTab3Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tab1Activity, Tab3Activity.class);
                startActivity(intent);
            }
        });

    }
}
