package edu.usc.emanuel_azage.custumtab;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Tab2Activity extends AppCompatActivity {

    private Button mTab1Button;
    private Button mTab2Button;
    private Button mTab3Button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab2);

        mTab1Button = (Button)findViewById(R.id.tab1Button);
        mTab2Button = (Button)findViewById(R.id.tab2Button);
        mTab3Button = (Button)findViewById(R.id.tab3Button);

        //if btn2 is pressed, refresh
        //if btn1 is pressed, go to tab1
        //if btn3 is pressed, go to tab3

        final Context Tab2Activity = this;
        mTab1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tab2Activity, Tab1Activity.class);
                startActivity(intent);
            }
        });

        mTab3Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tab2Activity, Tab3Activity.class);
                startActivity(intent);
            }
        });


    }
}
