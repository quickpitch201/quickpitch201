package com.example.abreezy.quickpitchproject.Entrepreneur;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.abreezy.quickpitchproject.R;

import BackEnd.Client.EntrepreneurAccount;
import BackEnd.Client.QuickPitchClient;

/**
 * Created by Abrar on 4/8/16.
 */
public class EntProfileActivity extends AppCompatActivity{

    private QuickPitchClient clientInstance;
    private EntrepreneurAccount myEntAccount;
    private Button mProfileButton;
    private Button mConnectionsButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.example.abreezy.quickpitchproject.R.layout.activity_ent_profile);

        mProfileButton = (Button)findViewById(com.example.abreezy.quickpitchproject.R.id.entProfileButton);
        mProfileButton.getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);

        mConnectionsButton = (Button)findViewById(R.id.entConnectionsButton);

        clientInstance = QuickPitchClient.getInstance();
        myEntAccount = clientInstance.getMyEntAccount();
        Log.d("MyAccountInfo", myEntAccount.getUsername());
        Log.d("MyAccountInfo", myEntAccount.getPassword());
        Log.d("MyAccountInfo", myEntAccount.getEmail());
        Log.d("MyAccountInfo", myEntAccount.getCompanyName());
        Log.d("MyAccountInfo", myEntAccount.getCompanyInfo()); //This is actually Product Name
        Log.d("MyAccountInfo", myEntAccount.getProductDescription());

        createTabs();
    }


    private void createTabs() {
        final Context thisActivity = this;

        mConnectionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(thisActivity, EntConnectionActivity.class);
                startActivity(intent);
                overridePendingTransition(com.example.abreezy.quickpitchproject.R.anim.push_out_left, com.example.abreezy.quickpitchproject.R.anim.pull_in_right);
            }
        });
    }

}