package BackEnd.Resources;

// import statements
import java.io.Serializable;

// GrabNewsFeedAttempt
public class GrabNewsFeedAttempt implements Serializable {
	
	// private member variables
	private static final long serialVersionUID = 1L;
	private String entrepreneur;
	
	// Constructor
	public GrabNewsFeedAttempt(String entrepreneur) {
		this.entrepreneur = entrepreneur;
	}
	
	// encapsulation methods
	public String getEntrepreneur() {
		return entrepreneur;
	}
	
	public void setEntrepreneur(String entrepreneur) {
		this.entrepreneur = entrepreneur;
	}
}
