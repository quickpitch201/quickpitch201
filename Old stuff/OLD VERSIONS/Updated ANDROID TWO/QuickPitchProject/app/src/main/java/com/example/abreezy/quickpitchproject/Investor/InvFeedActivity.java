package com.example.abreezy.quickpitchproject.Investor;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.VideoView;

public class InvFeedActivity extends AppCompatActivity {

    private Button mProfileButton;
    private Button mFeedButton;
    private Button mConnectionsButton;
    private Button mLikesButton;
    private VideoView myVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.example.abreezy.quickpitchproject.R.layout.activity_inv_feed);

        mFeedButton = (Button)findViewById(com.example.abreezy.quickpitchproject.R.id.FeedButton);
        mFeedButton.getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);

        mProfileButton = (Button)findViewById(com.example.abreezy.quickpitchproject.R.id.invProfileButton);
        mConnectionsButton = (Button)findViewById(com.example.abreezy.quickpitchproject.R.id.invConnectionsButton);
        mLikesButton = (Button)findViewById(com.example.abreezy.quickpitchproject.R.id.likesButton);



        createTabs();
    }

    private void createTabs(){
        final Context thisActivity = this;

        mConnectionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(thisActivity, InvConnectionsActivity.class);
                startActivity(intent);
                overridePendingTransition(com.example.abreezy.quickpitchproject.R.anim.push_out_left, com.example.abreezy.quickpitchproject.R.anim.pull_in_right);
            }
        });

        mProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(thisActivity, InvProfileActivity.class);
                startActivity(intent);
                overridePendingTransition(com.example.abreezy.quickpitchproject.R.anim.push_out_right, com.example.abreezy.quickpitchproject.R.anim.pull_in_left);
            }
        });

        mLikesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(thisActivity, InvLikesActivity.class);
                startActivity(intent);
                overridePendingTransition(com.example.abreezy.quickpitchproject.R.anim.push_out_right, com.example.abreezy.quickpitchproject.R.anim.pull_in_left);
            }
        });
    }
}
