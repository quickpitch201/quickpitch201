package com.example.abreezy.quickpitchproject.Investor;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.abreezy.quickpitchproject.R;

/**
 * wasn't created by Emanuel_Azage on 4/7/16.
 */

public class InvConnectionsActivity extends AppCompatActivity{
    private Button mProfileButton;
    private Button mFeedButton;
    private Button mLikesButton;
    private Button mConnectionsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.example.abreezy.quickpitchproject.R.layout.activity_inv_connections);

        mProfileButton = (Button)findViewById(com.example.abreezy.quickpitchproject.R.id.invProfileButton);
        mFeedButton = (Button)findViewById(com.example.abreezy.quickpitchproject.R.id.FeedButton);
        mLikesButton = (Button)findViewById(com.example.abreezy.quickpitchproject.R.id.likesButton);

        mConnectionsButton = (Button)findViewById(com.example.abreezy.quickpitchproject.R.id.invConnectionsButton);




        createTab();

        populateListView();
    }

    private void createTab(){
        final Context thisActivity = this;

        mProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(thisActivity, InvProfileActivity.class);
                startActivity(intent);
                overridePendingTransition(com.example.abreezy.quickpitchproject.R.anim.push_out_right, com.example.abreezy.quickpitchproject.R.anim.pull_in_left);
            }
        });

        mFeedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(thisActivity, InvFeedActivity.class);
                startActivity(intent);
                overridePendingTransition(com.example.abreezy.quickpitchproject.R.anim.push_out_right, com.example.abreezy.quickpitchproject.R.anim.pull_in_left);
            }
        });

        mLikesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(thisActivity, InvLikesActivity.class);
                startActivity(intent);
                overridePendingTransition(com.example.abreezy.quickpitchproject.R.anim.push_out_right, com.example.abreezy.quickpitchproject.R.anim.pull_in_left);
            }
        });
    }

    private void populateListView(){
        //create List of Items
        String[] entList = {"Ent1","Ent2", "Ent3", "Ent4", "Ent5", "Ent6", "Ent7", "Ent8" };

        // Build Adapter
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                com.example.abreezy.quickpitchproject.R.layout.items,
                entList
        );

        //Configure the list view
        ListView connectionsList = (ListView)findViewById(R.id.connectionsListView);
        connectionsList.setAdapter(adapter);

    }
}
