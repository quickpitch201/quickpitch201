package BackEnd.Server;

// import statements
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;
import java.util.Vector;
import BackEnd.Client.EntrepreneurAccount;
import BackEnd.Client.InvestorAccount;
import BackEnd.Resources.AddConnectionAttempt;
import BackEnd.Resources.AddLikeAttempt;
import BackEnd.Resources.EntSignUpAttempt;
import BackEnd.Resources.GrabNewsFeedAttempt;
import BackEnd.Resources.InvSignUpAttempt;
import BackEnd.Resources.LoginAttempt;

// QuickPitchJDBC 
public class QuickPitchJDBC {
	
	// private member variables
	private Connection conn;
	private ResultSet rs;

	// Constructor
	public QuickPitchJDBC() {
		
		// sets up database including connection, result set and JDBC driver
		conn = null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost/QuickPitchData?user=root&password=root");
			rs = null;
		
		// catches errors
		} catch (SQLException sqle) {
			System.out.println("sqle: " + sqle.getMessage());
			sqle.printStackTrace();
		} catch (ClassNotFoundException cnfe) {
			System.out.println("cnfe: " + cnfe.getMessage());
			cnfe.printStackTrace();
		}
	}
	
	// adds an investor account data to JDBC
	public InvestorAccount insertInvestor(InvSignUpAttempt attempt) {
		
		// gets encapsulated private member variables from invSignUpAttempt
		String username = attempt.getUsername();
		String password = attempt.getPassword();
		String email = attempt.getEmail();
		String companyName = attempt.getCompanyName();
		String accountType = "Investors";
		
		try {
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM Investors WHERE username=?");
			ps.setString(1, username);
			rs = ps.executeQuery();
			if(rs.next()) {
				rs.close();
				ps.close();
				return null;
			}
			else {
				ps = conn.prepareStatement("INSERT INTO UserAccountTypes (username, accountType) "
											+ "VALUES (?,?);");
				ps.setString(1, username);
				ps.setString(2, accountType);
				ps.executeUpdate();
				ps.close();
				
				ps = conn.prepareStatement("INSERT INTO Investors (username, pword, email, companyName) "
											+ "VALUES (?,?,?,?);");
				ps.setString(1, username);
				ps.setString(2, password);
				ps.setString(3, email);
				ps.setString(4, companyName);
				ps.executeUpdate();
				rs.close();
				ps.close();
				InvestorAccount invAccount = new InvestorAccount(username, password, email, companyName);
				return invAccount;
			}
		// catches errors
		} catch (SQLException sqle) {
			System.out.println("sqle: " + sqle.getMessage());
			sqle.printStackTrace();
		}
		return null;
	}
	
	// adds entrepreneur account data to JDBC
	public EntrepreneurAccount insertEntrepreneur(EntSignUpAttempt attempt) {
		
		// gets encapsulated private member variables from entSignUpAttempt
		String username = attempt.getUsername();
		String password = attempt.getPassword();
		String email = attempt.getEmail();
		String companyName = attempt.getCompanyName();
		String companyInfo = attempt.getCompanyInfo();
		String productDescription = attempt.getProductDescription();
		String videoURL = attempt.getVideoURL();
		String accountType = "Entrepreneurs";
		
		try {
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM Entrepreneurs WHERE username=?");
			ps.setString(1, username);
			rs = ps.executeQuery();
			if(rs.next()) {
				rs.close();
				ps.close();
				return null;
			}
			else {
				ps = conn.prepareStatement("INSERT INTO UserAccountTypes (username, accountType) "
											+ "VALUES (?,?);");
				ps.setString(1, username);
				ps.setString(2, accountType);
				ps.executeUpdate();
				ps.close();
				
				ps = conn.prepareStatement("INSERT INTO Entrepreneurs (username, pword, email, companyName, companyInfo, productDescription, videoURL) "
											+ "VALUES (?,?,?,?,?,?,?);");
				ps.setString(1, username);
				ps.setString(2, password);
				ps.setString(3, email);
				ps.setString(4, companyName);
				ps.setString(5, companyInfo);
				ps.setString(6, productDescription);
				ps.setString(7, videoURL);
				ps.executeUpdate();
				rs.close();
				ps.close();
				EntrepreneurAccount entAccount = new EntrepreneurAccount(username, password, email, companyName, companyInfo, productDescription, videoURL);
				return entAccount;
			}
		// catches errors
		} catch (SQLException sqle) {
			System.out.println("sqle: " + sqle.getMessage());
		}
		return null;
	}
	
	// checks a username and password login of user
	public Object checkLogin(LoginAttempt attempt){
		String username = attempt.getUsername();
		String password = attempt.getPassword();
		try {
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM UserAccountTypes WHERE username=?");
			ps.setString(1, username);
			rs = ps.executeQuery();
			if (rs.next()){
				String accountType = rs.getString("accountType");
				if(accountType.equals("Investors")){
					ps = conn.prepareStatement("SELECT * FROM Investors WHERE username=? AND pword=?");
					ps.setString(1, username);
					ps.setString(2, password);
					rs = ps.executeQuery();
					if(rs.next()){
						String email = rs.getString("email");
						String companyName = rs.getString("companyName");
						InvestorAccount investorAccount = new InvestorAccount(username, password, email, companyName);
						rs.close();
						ps.close();
						return investorAccount;
					}
					else {
						rs.close();
						ps.close();
						return null;
					}
				}
				else if (accountType.equals("Entrepreneurs")){
					ps.close();
					ps = conn.prepareStatement("SELECT * FROM Entrepreneurs WHERE username=? AND pword=?");
					ps.setString(1, username);
					ps.setString(2, password);
					rs = ps.executeQuery();
					if (rs.next()){
						String email = rs.getString("email");
						String companyName = rs.getString("companyName");
						String companyInfo = rs.getString("companyInfo");
						String productDescription = rs.getString("productDescription");
						String videoURL = rs.getString("videoURL");
						EntrepreneurAccount entrepreneurAccount = new EntrepreneurAccount(username, password, email, 
								companyName, companyInfo, productDescription, videoURL);
						rs.close();
						ps.close();
						return entrepreneurAccount;
					}
					else {
						rs.close();
						ps.close();
						return null;
					}
				}
			} 
			else {
				rs.close();
				ps.close();
				return null;
			}
		} catch(SQLException sqle){
			System.out.println("sqle from JDBC.checkLogin(): " + sqle.getMessage());
			sqle.printStackTrace();
		}
		return null;
	}
	
	// adds a connection between investor and entrepreneur
	public Boolean addConnection(AddConnectionAttempt attempt) {
		String investor = attempt.getInvestor();
		String entrepreneur = attempt.getEntrepreneur();
		Boolean successfulConnection = false;
		try {
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM LikesConnections WHERE investor=? and entrepreneur=? and likeOrConnection=?");
			ps.setString(1, investor);
			ps.setString(2, entrepreneur);
			ps.setString(3, "connection");
			rs = ps.executeQuery();
			if(rs.next()) {
				return false;
			}
			rs.close();
			ps.close();
			ps = conn.prepareStatement("INSERT INTO LikesConnections (investor, entrepreneur, likeOrConnection) "
										+ "VALUES (?,?,?);");
			ps.setString(1, investor);
			ps.setString(2, entrepreneur);
			ps.setString(3, "connection");
			int rowsAffected = ps.executeUpdate();
			if (rowsAffected > 0){
				successfulConnection = true;
			}
			ps.close();
		} catch (SQLException sqle) {
			System.out.println("sqle: " + sqle.getMessage());
		}
		return successfulConnection;
	}
	
	// grabs a random entrepreneur account to display on news feed
	public EntrepreneurAccount grabEntrepreneurAccount(GrabNewsFeedAttempt attempt) {
		Vector<String> entrepreneurs = new Vector<String>();
		try {
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM Entrepreneurs");
			rs = ps.executeQuery();
			while(rs.next()) {
				String username = rs.getString("username");
				entrepreneurs.addElement(username);
			}
			rs.close();
			ps.close();
			
			Random rand = new Random();
			int value = rand.nextInt(entrepreneurs.size());
			String selectedEntrepreneur = entrepreneurs.elementAt(value);
			ps = conn.prepareStatement("SELECT * FROM Entrepreneurs WHERE username=?");
			ps.setString(1, selectedEntrepreneur);
			rs = ps.executeQuery();
			if(rs.next()) {
				String username = rs.getString("username");
				String password = rs.getString("pword");
				String email = rs.getString("email");
				String companyName = rs.getString("companyName");
				String companyInfo = rs.getString("companyInfo");
				String productDescription = rs.getString("productDescription");
				String videoURL = rs.getString("videoURL");
				EntrepreneurAccount entrepreneurAccount = new EntrepreneurAccount(username, password, email, companyName, companyInfo, productDescription, videoURL);
				return entrepreneurAccount;
			}
			else {
				return null;
			}
		} catch (SQLException sqle) {
			System.out.println("sqle from JDBC.grabEntrepreneurAccount(); " + sqle.getMessage());
			sqle.printStackTrace();
		}
		return null;
	}
	
	// adds like between investor and entrepreneur
	public Boolean addLike(AddLikeAttempt attempt) {
		String investor = attempt.getInvestor();
		String entrepreneur = attempt.getEntrepreneur();
		Boolean successfulLike = false;
		try {
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM LikesConnections WHERE investor=? and entrepreneur=? and likeOrConnection=?");
			ps.setString(1, investor);
			ps.setString(2, entrepreneur);
			ps.setString(3, "like");
			rs = ps.executeQuery();
			if(rs.next()) {
				return false;
			}
			rs.close();
			ps.close();
			ps = conn.prepareStatement("INSERT INTO LikesConnections (investor, entrepreneur, likeOrConnection) "
														+ "VALUES (?,?,?);");
			ps.setString(1, investor);
			ps.setString(2, entrepreneur);
			ps.setString(3, "like");
			int rowsAffected = ps.executeUpdate();
			if (rowsAffected > 0){
				successfulLike = true;
			}
			ps.close();
		} catch (SQLException sqle) {
			System.out.println("sqle: " + sqle.getMessage());
		}
		return successfulLike;
	}
	
	// main function for testing purposes only
	public static void main(String [] args) {
		QuickPitchJDBC myJDBC = new QuickPitchJDBC();
		InvSignUpAttempt invSignUpAttempt = new InvSignUpAttempt("Srivas", "password", "stirumal@usc.edu", "QuickPitch");
		myJDBC.insertInvestor(invSignUpAttempt);
		EntSignUpAttempt entSignUpAttempt = new EntSignUpAttempt("Tony", "password", "tua@usc.edu", "QuickPitch", "we're hot", "sex machine", "video.com");
		myJDBC.insertEntrepreneur(entSignUpAttempt);
		LoginAttempt loginAttempt1 = new LoginAttempt("Srivas", "password");
		InvestorAccount invAccount = (InvestorAccount)myJDBC.checkLogin(loginAttempt1);
		System.out.println("investor account: " + invAccount.getUsername());
		LoginAttempt loginAttempt2 = new LoginAttempt("Tony", "password");
		EntrepreneurAccount entAccount = (EntrepreneurAccount)myJDBC.checkLogin(loginAttempt2);
		System.out.println("entrepreneur account: " + entAccount.getUsername());
		AddLikeAttempt addLikeAttempt = new AddLikeAttempt("Tony", "Srivas");
		Boolean success1 = myJDBC.addLike(addLikeAttempt);
		System.out.println("like added: " + success1);
		AddConnectionAttempt addConnectionAttempt = new AddConnectionAttempt("Tony", "Srivas");
		Boolean success2 = myJDBC.addConnection(addConnectionAttempt);
		System.out.println("connection added: " + success2);		
		Boolean success3 = myJDBC.addConnection(addConnectionAttempt);
		System.out.println("connection added : " + success3);
	}
}

