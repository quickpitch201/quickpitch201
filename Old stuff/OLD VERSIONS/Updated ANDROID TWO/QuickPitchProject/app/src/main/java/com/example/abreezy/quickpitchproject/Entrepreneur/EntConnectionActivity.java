package com.example.abreezy.quickpitchproject.Entrepreneur;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.abreezy.quickpitchproject.R;

/**
 * Created by Abrar on 4/9/16.
 */
public class EntConnectionActivity extends AppCompatActivity {

    private Button mProfileButton;
    private Button mConnectionsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.example.abreezy.quickpitchproject.R.layout.activity_ent_connections);

        mProfileButton = (Button)findViewById(com.example.abreezy.quickpitchproject.R.id.entProfileButton);

        mConnectionsButton = (Button)findViewById(R.id.entConnectionsButton);
        mConnectionsButton.getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);



        createTab();

        populateListView();
    }


    private void createTab(){
        final Context thisActivity = this;

        mProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(thisActivity, EntProfileActivity.class);
                startActivity(intent);
                overridePendingTransition(com.example.abreezy.quickpitchproject.R.anim.push_out_right, com.example.abreezy.quickpitchproject.R.anim.pull_in_left);
            }
        });

    }

    private void populateListView(){
        //create List of Items
        String[] entList = {"Inv1","Inv2", "Inv3", "Inv4", "Inv5", "Inv6", "Inv7", "Inv8" };

        // Build Adapter
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                com.example.abreezy.quickpitchproject.R.layout.items,
                entList
        );

        //Configure the list view
        ListView connectionsList = (ListView)findViewById(R.id.entConnectionsListView);
        connectionsList.setAdapter(adapter);

    }

}
