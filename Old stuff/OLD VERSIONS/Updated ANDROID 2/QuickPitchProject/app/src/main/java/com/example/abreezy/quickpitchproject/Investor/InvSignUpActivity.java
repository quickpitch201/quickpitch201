package com.example.abreezy.quickpitchproject.Investor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.abreezy.quickpitchproject.R;

public class InvSignUpActivity extends AppCompatActivity {
    private Button mCreateButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inv_sign_up);
        mCreateButton = (Button)findViewById(com.example.abreezy.quickpitchproject.R.id.nextButton);
        mCreateButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                    Intent intent = new Intent(InvSignUpActivity.this, InvProfileActivity.class);
                    startActivity(intent);
            }
        });
    }
}
