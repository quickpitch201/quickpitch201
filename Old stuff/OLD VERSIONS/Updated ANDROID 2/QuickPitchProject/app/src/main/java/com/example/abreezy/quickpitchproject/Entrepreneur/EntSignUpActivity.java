package com.example.abreezy.quickpitchproject.Entrepreneur;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.abreezy.quickpitchproject.R;

public class EntSignUpActivity extends AppCompatActivity {
    private Button mNextButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ent_sign_up);

        mNextButton = (Button)findViewById(com.example.abreezy.quickpitchproject.R.id.nextButton);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                    Intent intent = new Intent(EntSignUpActivity.this, EntSecondSignUpActivity.class);
                    startActivity(intent);
            }
        });
    }
}
