package client;

// import statements
import java.util.Scanner;
import resources.AddConnectionAttempt;
import resources.AddLikeAttempt;
import resources.EntSignUpAttempt;
import resources.GrabNewsFeedAttempt;
import resources.InvSignUpAttempt;
import resources.LoginAttempt;

// TestClass
public class Test {

		// main method
		public static void main(String [] args) {
			
			// initialize and get user input
			QuickPitchClient testClient = new QuickPitchClient("localhost", 6789);
			Scanner scanner = new Scanner(System.in);
			int cas = 0;
			
			while(cas != 9) {
				System.out.print("Which case would you like to test?\n");  
				cas = scanner.nextInt(); // Get what the user types.
				
				// checks if investor can successfully sign up
				if (cas == 1) {
					InvSignUpAttempt invSignUpAttempt = new InvSignUpAttempt("uddhav", "root", "joglekar@usc.edu",
							"QuickPitch");
					InvestorAccount investorAccount = testClient.sendInvSignUpAttempt(invSignUpAttempt);
					
					if (investorAccount instanceof InvestorAccount) {
						System.out.println(investorAccount.getUsername());
						System.out.println(investorAccount.getPassword());
						System.out.println(investorAccount.getEmail());
						System.out.println(investorAccount.getCompanyName());
					}
					else if (investorAccount == null) {
						System.out.println("The investor account was not created correctly.");
					}
					else {
						System.out.println("Test.main(): This line of code should never print...");
					}
					
				}
				
				// checks if entrepreneur can successfully sign up
				else if (cas == 2) {
					EntSignUpAttempt entSignUpAttempt = new EntSignUpAttempt("tony", "csci", "tua@usc.edu",
							"QuickPitch", "Technology Firm", "Andorid App", "videoURL");
					EntrepreneurAccount entrepreneurAccount = testClient.sendEntSignUpAttempt(entSignUpAttempt);
					
					if (entrepreneurAccount instanceof EntrepreneurAccount) {
						System.out.println(entrepreneurAccount.getUsername());
						System.out.println(entrepreneurAccount.getPassword());
						System.out.println(entrepreneurAccount.getEmail());
						System.out.println(entrepreneurAccount.getCompanyName());
						System.out.println(entrepreneurAccount.getCompanyInfo());
						System.out.println(entrepreneurAccount.getProductDescription());
						System.out.println(entrepreneurAccount.getVideo());
					}
					else if (entrepreneurAccount == null) {
						System.out.println("The entrepreneur account was not created correctly.");
					}
					else {
						System.out.println("Test.main(): This line of code should never print...");
					}
				}
				
				// adds a second entrepreneur account
				else if (cas == 3) {
					EntSignUpAttempt entSignUpAttempt = new EntSignUpAttempt("srivas", "201", "stirumal@usc.edu",
							"Jack Of fMan", "I make Mopeds", "Motocycles", "videoURL");
					EntrepreneurAccount entrepreneurAccount = testClient.sendEntSignUpAttempt(entSignUpAttempt);
					
					if (entrepreneurAccount instanceof EntrepreneurAccount) {
						System.out.println(entrepreneurAccount.getUsername());
						System.out.println(entrepreneurAccount.getPassword());
						System.out.println(entrepreneurAccount.getEmail());
						System.out.println(entrepreneurAccount.getCompanyName());
						System.out.println(entrepreneurAccount.getCompanyInfo());
						System.out.println(entrepreneurAccount.getProductDescription());
						System.out.println(entrepreneurAccount.getVideo());
					}
					else if (entrepreneurAccount == null) {
						System.out.println("The entrepreneur account was not created correctly.");
					}
					else {
						System.out.println("Test.main(): This line of code should never print...");
					}
				}
				
				// checks if ambiguous account can sign in successfully
				else if (cas == 4) {
					LoginAttempt loginAttempt = new LoginAttempt("uddhav", "root");
					Object account = testClient.sendLoginAttempt(loginAttempt);
					
					if (account instanceof InvestorAccount) {
						InvestorAccount investorAccount = (InvestorAccount)account;
						System.out.println(investorAccount.getUsername());
						System.out.println(investorAccount.getPassword());
						System.out.println(investorAccount.getEmail());
						System.out.println(investorAccount.getCompanyName());
					}
					else if (account instanceof EntrepreneurAccount) {
						EntrepreneurAccount entrepreneurAccount = (EntrepreneurAccount)account;
						System.out.println(entrepreneurAccount.getUsername());
						System.out.println(entrepreneurAccount.getPassword());
						System.out.println(entrepreneurAccount.getEmail());
						System.out.println(entrepreneurAccount.getCompanyName());
						System.out.println(entrepreneurAccount.getCompanyInfo());
						System.out.println(entrepreneurAccount.getProductDescription());
						System.out.println(entrepreneurAccount.getVideo());
					}
					else if (account == null) {
						System.out.println("This username and password does not exist.");
					}
					else {
						System.out.println("Test.main(): This line of code should never print...");
					}
				}
				
				else if (cas == 5) {
					LoginAttempt loginAttempt = new LoginAttempt("uddhav", "fuckme");
					Object account = testClient.sendLoginAttempt(loginAttempt);
					
					if (account instanceof InvestorAccount) {
						InvestorAccount investorAccount = (InvestorAccount)account;
						System.out.println(investorAccount.getUsername());
						System.out.println(investorAccount.getPassword());
						System.out.println(investorAccount.getEmail());
						System.out.println(investorAccount.getCompanyName());
					}
					else if (account instanceof EntrepreneurAccount) {
						EntrepreneurAccount entrepreneurAccount = (EntrepreneurAccount)account;
						System.out.println(entrepreneurAccount.getUsername());
						System.out.println(entrepreneurAccount.getPassword());
						System.out.println(entrepreneurAccount.getEmail());
						System.out.println(entrepreneurAccount.getCompanyName());
						System.out.println(entrepreneurAccount.getCompanyInfo());
						System.out.println(entrepreneurAccount.getProductDescription());
						System.out.println(entrepreneurAccount.getVideo());
					}
					else if (account == null) {
						System.out.println("This username and password does not exist.");
					}
					else {
						System.out.println("Test.main(): This line of code should never print...");
					}
				}
				
				// checks if news feed can grab an entrepreneur account
				else if (cas == 6) {
					GrabNewsFeedAttempt grabNewsFeedAttempt = new GrabNewsFeedAttempt("uddhav");
					EntrepreneurAccount entrepreneurAccount = testClient.grabNewsfeed(grabNewsFeedAttempt);
					
					if (entrepreneurAccount instanceof EntrepreneurAccount) {
						System.out.println(entrepreneurAccount.getUsername());
						System.out.println(entrepreneurAccount.getPassword());
						System.out.println(entrepreneurAccount.getEmail());
						System.out.println(entrepreneurAccount.getCompanyName());
						System.out.println(entrepreneurAccount.getCompanyInfo());
						System.out.println(entrepreneurAccount.getProductDescription());
						System.out.println(entrepreneurAccount.getVideo());
					}
					else if(entrepreneurAccount == null) {
						System.out.println("There was no entrepreneur account returned.");
					}
					else {
						System.out.println("Test.main(): This line of code should never print...");
					}
				}
				
				// checks if account can add connection
				else if (cas == 7) {
					AddConnectionAttempt addConnectionAttempt = new AddConnectionAttempt("tony", "uddhav");
					Boolean result = testClient.addConnection(addConnectionAttempt);
					if (result) {
						System.out.println("result is true");
					} else {
						System.out.println("result is false");
					}
				}
				
				// checks if account can add like
				else if (cas == 8) {
					AddLikeAttempt addLikeAttempt = new AddLikeAttempt("tony", "uddhav");
					Boolean result = testClient.addLike(addLikeAttempt);
					if (result) {
						System.out.println("result is true");
					} else {
						System.out.println("result is false");
					}
				}
			}
			// close the scanner
			scanner.close();
		}
}
