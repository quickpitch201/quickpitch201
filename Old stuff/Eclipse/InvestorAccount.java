package client;

import java.io.Serializable;
import java.util.Vector;

public class InvestorAccount implements Serializable {
	
	// all private member variables needed for the InvestorAccount
	private static final long serialVersionUID = 1L;
	private String username;
	private String password; // probably don't actually want this to be saved
	private String email;
	private String companyName;
	private Vector<EntrepreneurAccount> likes;
	private Vector<EntrepreneurAccount> connections;
	
	// constructor
	public InvestorAccount(String username, String password, String email, String companyName) {
		likes = new Vector<EntrepreneurAccount>();
		connections = new Vector<EntrepreneurAccount>();
		this.username = username;
		this.password = password;
		this.email = email;
		this.companyName = companyName;
	}
	
	// encapsulation methods
	public Vector<EntrepreneurAccount> getLikes() {
		return likes;
	}
	
	public void setLikes(Vector<EntrepreneurAccount> likes) {
		this.likes = likes;
	}
	
	public Vector<EntrepreneurAccount> getConnections() {
		return connections;
	}
	
	public void setConnections(Vector<EntrepreneurAccount> connections) {
		this.connections = connections;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
}
