package client;

// import statements
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import resources.AddConnectionAttempt;
import resources.AddLikeAttempt;
import resources.EntSignUpAttempt;
import resources.GrabNewsFeedAttempt;
import resources.InvSignUpAttempt;
import resources.LoginAttempt;

public class QuickPitchClient extends Thread {
	
	// singleton model
	private static QuickPitchClient instance = null;
	
	public static QuickPitchClient getInstance(String hostname, int port) {
		if(instance == null) {
	         instance = new QuickPitchClient(hostname, port);
	      }
	      return instance;
	}
	
	// private member variables
	// input and output stream to communicate with the server
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private Socket s;
	private String hostname;
	private int port;
	private Object ambiguousAccount;
	private InvestorAccount invAccount;
	private EntrepreneurAccount entAccount;
	private boolean result;
	private boolean hasReceived;
	
	// constructor which takes the IPAddress and Port number
	public QuickPitchClient(String hostname, int port) {
		s = null;
		this.hostname = hostname;
		this.port = port;
		hasReceived = false;
		
		// starts the thread
		this.start();
		
	}
	
	public void run() {
		try {
			// setting up the socket and input and output streams
			s = new Socket(hostname, port);
			ois = new ObjectInputStream(s.getInputStream());
			oos = new ObjectOutputStream(s.getOutputStream());
			
			// introductory message to the server
			System.out.println("Connection to server...");
			oos.writeObject("Hey, Server, this is a Client calling.");
			oos.flush();

		} catch(IOException ioe) {
			System.out.println("ioe from QuickPitchClient(): " + ioe.getMessage());
			ioe.printStackTrace();
		} 
		
		try {
			while(true) {
				String message = (String)ois.readObject();
				System.out.println(message);
				
				if (message.equals("Hey Client, you're connected.")) {
					
				}
				
				if (message.equals("InvSignUpAttempt")) {
					invAccount = (InvestorAccount)ois.readObject();
					hasReceived = true;
				}
				else if (message.equals("EntSignUpAttempt")) {
					entAccount = (EntrepreneurAccount)ois.readObject();
					hasReceived = true;
				}
				else if (message.equals("LoginAttempt")) {
					ambiguousAccount = (Object)ois.readObject();
					hasReceived = true;
				}
				else if (message.equals("GrabNewsFeedAttempt")) {
					entAccount = (EntrepreneurAccount)ois.readObject();
					hasReceived = true;
				}
				else if (message.equals("AddConnectionAttempt")) {
					result = (boolean)ois.readObject();
					hasReceived = true;
				}
				else if (message.equals("AddLikeAttempt")) {
					result = (boolean)ois.readObject();
					hasReceived = true;
				}
			}		
		} catch(ClassNotFoundException cnfe) {
			System.out.println("cnfe from QuickPitchClient.run(): " + cnfe.getMessage());
			cnfe.printStackTrace();
		} catch (IOException ioe) {
			System.out.println("ioe from QuickPitchClient.run(): " + ioe.getMessage());
			ioe.printStackTrace();
		}
		
	}
	
	// all the client server communication methods
	public InvestorAccount sendInvSignUpAttempt(InvSignUpAttempt invSignUpAttempt) {
		
		invAccount = null;
		try {
			oos.writeObject(invSignUpAttempt);
			while(!hasReceived) {
				Thread.sleep(1000);
			}
			hasReceived = false;
		} catch (IOException ioe) {
			System.out.println("error from QuickPitchClient.sendInvSignUpAttempt(): " + ioe.getMessage());
			ioe.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return invAccount;
	}
	
	public EntrepreneurAccount sendEntSignUpAttempt(EntSignUpAttempt entSignUpAttempt) {
		
		entAccount = null;
		try {
			oos.writeObject(entSignUpAttempt);
			while(!hasReceived) {
				Thread.sleep(1000);
			}
			hasReceived = false;
		} catch (IOException ioe) {
			System.out.println("error from QuickPitchClient.sendEntSignUpAttempt(): " + ioe.getMessage());
			ioe.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return entAccount;
	}
	
	public Object sendLoginAttempt(LoginAttempt loginAttempt) {
		
		ambiguousAccount = null;
		try {
			oos.writeObject(loginAttempt);
			while(!hasReceived) {
				Thread.sleep(1000);
			}
			hasReceived = false;
		} catch (IOException ioe) {
			System.out.println("error from QuickPitchClient.sendLoginAttempt(): " + ioe.getMessage());
			ioe.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// return object
		return ambiguousAccount;
	}
	
	public EntrepreneurAccount grabNewsfeed(GrabNewsFeedAttempt grabNewsFeedAttempt) {
		
		entAccount = null;
		try {
			oos.writeObject(grabNewsFeedAttempt);
			while(!hasReceived) {
				Thread.sleep(1000);
			}
			hasReceived = false;
		} catch (IOException ioe) {
			System.out.println("error from QuickPitchClient.grabNewsFeed(): " + ioe.getMessage());
			ioe.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return entAccount;
	}
	
	public boolean addConnection(AddConnectionAttempt addConnectionAttempt) {
		result = false;
		try {
			oos.writeObject(addConnectionAttempt);
			while(!hasReceived) {
				Thread.sleep(1000);
			}
			hasReceived = false;
		} catch (IOException ioe) {
			System.out.println("error from QuickPitchClient.addConnection(): " + ioe.getMessage());
			ioe.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	public boolean addLike(AddLikeAttempt addLikeAttempt) {
		result = false;
		try {
			oos.writeObject(addLikeAttempt);
			while(!hasReceived) {
				Thread.sleep(1000);
			}
			hasReceived = false;
		} catch (IOException ioe) {
			System.out.println("error from QuickPitchClient.addLike(): " + ioe.getMessage());
			ioe.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return result;
	}

	// main method to run the client
	public static void main(String [] args) {

		@SuppressWarnings("unused")
		QuickPitchClient instance = new QuickPitchClient("10.0.23.11", 6789);
		
	}

}
