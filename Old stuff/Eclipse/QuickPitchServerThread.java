package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class QuickPitchServerThread extends Thread {
	//private member variables
	private Socket s;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private DBCommunicator database;
	
	public QuickPitchServerThread(Socket s){
		try{
			this.s = s;
			ois = new ObjectInputStream(s.getInputStream());
			oos = new ObjectOutputStream(s.getOutputStream());
			this.start();
		} catch(IOException ioe){
			System.out.println("ioe: " + ioe.getMessage());
		}
	}
	
	public void run(){
		try {
			while(true){
				Object object = ois.readObject();
				if (object instanceof InvSignUpAttempt){
					//send investor sign up attempt to database
					//verify if the sign up attempt is correct
					//then obtain an investor account with username and password
					
				}
				else if (object instanceof EntSignUpAttempt){
					//send entrepreneur sign up attempt to database
					//obtain an instance of entrepreneur account from DBCommunicator method
					//send EntrepreneurAccount to client side
				}
				else if (object instanceof LoginAttempt){
					
				}
			}
		}
	}
	
	private void sendInvSignUpAttempt(InvestorAccount investor){
		//sends the new investor account to the client side
		oos.writeObject(investor);
		oos.flush();
	}
	
	private void sendEntSignUpAttempt(EntrepreneurAccount entrepreneur){
		//sends the new entrepreneur account to the client side
		oos.writeObject(entrepreneur);
		oos.flush();
	}
	
	private void sendLoginAttempt(){
		//based on what the login attempt was, send back either an EntrepreneurAccount or InvestorAccount
	}
	
	private void grabNewsFeed(EntrepreneurAccount entrepreneur){
		//send back the EntrepreneurAccount one by one to the client
		oos.writeObject(entrepreneur);
		oos.flush();
	}
	
	private void addConnection(Boolean successfulConnection){
		//send the Boolean value of whether the DBCommunicator was able to add a connection
		oos.writeObject(successfulConnection);
		oos.flush();
	}
	
	private void addLike(Boolean successfulLike){
		//send the Boolena value of whether or not the DBCommunicator was able to add a like
		oos.writeObject(successfulLike);
		oos.flush();
	}	
}
