package server;

import java.sql.Connection;
import java.sql.Statement;

class DBCommunicator {
	//private member variables
	Statement st;
	Connection conn;
	
	
	//constructor for DBCommunicator
	protected DBCommunicator(){
		conn = null;
		st = null;
		//create connection with the database
		//create statement based off connection
	}
	
	protected InvestorAccount insertInvestor(InvSignUpAttempt attempt){
		//verify if the sign up attempt is valid
		//create a new instance of the investor account and populate the according fields
		//return an InvestorAccount
	}
	
	protected EntrepreneurAccount insertEntrepreneur(EntSignUpAttempt attempt){
		//verify if the entrepreneur sign up attempt is valid
		//create a new instance of the entrepreneur account and populate the according fields
		//return the instance of the entrepreneur account
	}
	
	protected ParentAccount checkLogin(LoginAttempt attempt){
		//check if user is a valid entrepreneur or investor
		//return a parent account of corresponding information
		
	}
	
	protected EntrepreneurAccount grabEntrepreneurAccount(GrabNewsFeedAttempt NewsAttempt){
		//return the corresponding entrepreneur account 
	}
	
	
	protected Boolean addConnection(AddConnectionAttempt ConnectionAttempt){
		//add a connection to a corresponding investor
		//return boolean if able to successfully add connection or not
	}
	
	protected Boolean addLike(AddLikeAttempt LikeAttempt){
		//add a like to the corresponding investor
		//return boolean if able to successfully add like or not
	}
}
