SELECT * FROM QuickPitchData.Entrepreneurs;

INSERT INTO QuickPitchData.Entrepreneurs VALUES
('coin', '6$', 'coin@gmail.com', 'Coin', 'Coin 2.0',
'Use the coin to combine all your credit cards into one! No point in carrying around all of your cards. Condense it all into the coin!',
'https://www.youtube.com/watch?v=w9Sx34swEG0');

INSERT INTO QuickPitchData.UserAccountTypes VALUES
('coin', 'Entrepreneurs');

INSERT INTO QuickPitchData.Entrepreneurs VALUES
('ouya', 'B$', 'ouya@gmail.com', 'Ouya', 'Ouya Controller',
'OUYA is an open, free-to-play game console that upends the console market by putting the power back in the hands of devs and gamers. OUYA offers every game free to try because we believe you should not buy anything until you know you love it.',
'https://www.youtube.com/watch?v=U39L4mEyIRc');

INSERT INTO QuickPitchData.UserAccountTypes VALUES
('ouya', 'Entrepreneurs');

INSERT INTO QuickPitchData.Entrepreneurs VALUES
('atom', '4$', 'atom@gmail.com', 'Siva Cycle', 'The Atom',
'The Siva Atom is a lightweight USB generator to charge any of your personal electronics as you cycle. Easy and intuitive to install, you can charge while riding or once you arrive, thanks to the 1650mAh removable battery pack.',
'https://www.youtube.com/watch?v=zcnY-mcw4zc');

INSERT INTO QuickPitchData.UserAccountTypes VALUES
('atom', 'Entrepreneurs');

INSERT INTO QuickPitchData.Entrepreneurs VALUES
('petcube', 'C$', 'petcube@gmail.com', 'Petcube', 'Petcube',
'Petcube is an interactive wireless pet camera that helps you stay connected to your pets when you are not at home. Petcube pet monitor system lets you watch, talk and play laser games with your dog or cat from anywhere using your smartphone. You can share access to your pet video cam with your friends, family or anyone else on Petcube network.',
'https://www.youtube.com/watch?v=RlFVx-grsqY');

INSERT INTO QuickPitchData.UserAccountTypes VALUES
('petcube', 'Entrepreneurs');

INSERT INTO QuickPitchData.Entrepreneurs VALUES
('readyset', 'E$', 'readyset@gmail.com', 'Fenix International', 'The Ready Set',
'ReadySet is a solar charger that can power up to 10 iPhones or run an iPad for 12 hours of continuous video-play on a single charge.',
'https://www.youtube.com/watch?v=RyOpJYElRSM');

INSERT INTO QuickPitchData.UserAccountTypes VALUES
('readyset', 'Entrepreneurs');

INSERT INTO QuickPitchData.Entrepreneurs VALUES
('hoverbike', ';$', 'hoverbike@gmail.com', 'Chris Malloy', 'Hoverbike',
'Hoverbike is a revolution in aviation, designed to do what a helicopter does, but cost less and do so better',
'https://www.youtube.com/watch?v=t7U-DI5epEM');

INSERT INTO QuickPitchData.UserAccountTypes VALUES
('hoverbike', 'Entrepreneurs');

INSERT INTO QuickPitchData.Entrepreneurs VALUES
('stesco', 'F$', 'stesco@gmail.com', 'Moebius Industries LTD', 'Stesco',
'Use Stesco and the Stesco App to Make high quality 3D stereoscopic photos and videos with two iPhones. Easy to use, amazing to watch.',
'https://www.youtube.com/watch?v=tnHelrwUINk');

INSERT INTO QuickPitchData.UserAccountTypes VALUES
('stesco', 'Entrepreneurs');

INSERT INTO QuickPitchData.Entrepreneurs VALUES
('breton', '5$', 'breton@gmail.com', 'Breton Company', 'Breton Bags',
'A bag for the modern professional, combining the comfort and convenience of a backpack, with the style and look of a high-end satchel.',
'https://www.youtube.com/watch?v=_neK8k1KuUM');

INSERT INTO QuickPitchData.UserAccountTypes VALUES
('breton', 'Entrepreneurs');

INSERT INTO QuickPitchData.Entrepreneurs VALUES
('thanotech', 'G$', 'thanotech@gmail.com', 'ThanoTech', 'K11 Bumper',
'Reimagining the bumper for your iPhone. Finally someone thought about it...',
'https://www.youtube.com/watch?v=nUfC4zDUX4E');

INSERT INTO QuickPitchData.UserAccountTypes VALUES
('thanotech', 'Entrepreneurs');

INSERT INTO QuickPitchData.Entrepreneurs VALUES
('onefastcat', 'B$', 'onefastcat@gmail.com', 'One Fast Cat', 'Cat Excersize Wheel',
'Our Kickstarter campaign was successful! We raised over 3,000% of our initial goal and the Cat Exercise Wheel has been a success!',
'https://www.youtube.com/watch?v=Zsq-8DUTYHg');

INSERT INTO QuickPitchData.UserAccountTypes VALUES
('onefastcat', 'Entrepreneurs');

INSERT INTO QuickPitchData.Entrepreneurs VALUES
('robox', 'E$', 'robox@gmail.com', 'C Enterprise', 'Robox 3D',
'CEL Video launched our Kickstarter Campaign, we hope to raise money to finish the robox 3D printer project and gain a Beta test group to give us much needed feedback.',
'https://www.youtube.com/watch?v=mpo91FpbGjM');

INSERT INTO QuickPitchData.UserAccountTypes VALUES
('robox', 'Entrepreneurs');

INSERT INTO QuickPitchData.Entrepreneurs VALUES
('bundlebeds', '5$', 'bundlebeds@gmail.com', 'Bundle Beds', 'Bundle Bed',
'The Bundle Beds Kickstarter campaign (link above) was launched in February 2016 - this is the official campaign video - featuring Lucy Bartlett and James Clark, the creators of the Bundle Bed. Be sure to follow the link to find out more about the product.',
'https://www.youtube.com/watch?v=U_HhtZEL5T8');

INSERT INTO QuickPitchData.UserAccountTypes VALUES
('bundlebeds', 'Entrepreneurs');

INSERT INTO QuickPitchData.Entrepreneurs VALUES
('revolights', 'E$', 'revolights@gmail.com', 'Revolights', 'Revolights',
'Revolights Wheels. Riding on their previously designed, developed, manufactured and now-patented Revolights bike lighting system from the first project, this second project will make internationally available easy-installation Tron-lit wheels that create the same 360 degree visibility and forward projection as before.',
'https://www.youtube.com/watch?v=We5yGHh1QJ4');

INSERT INTO QuickPitchData.UserAccountTypes VALUES
('revolights', 'Entrepreneurs');

INSERT INTO QuickPitchData.Entrepreneurs VALUES
('bubl', '5$', 'bubl@gmail.com', 'Bubl Technology Inc', 'Bublcam',
'The bublcam is a 360º camera and software technology that shares everything around you through spherical photos and videos.',
'https://www.youtube.com/watch?v=puNepKjwHJM');

INSERT INTO QuickPitchData.UserAccountTypes VALUES
('bubl', 'Entrepreneurs');

INSERT INTO QuickPitchData.Entrepreneurs VALUES
('bios', '5$', 'bios@gmail.com', 'Bios Urn', 'Bios Incube',
'Bios Incube is the world´s first system designed to help grow the remains of your loved ones into trees.',
'https://www.youtube.com/watch?v=yPPBB1QPP88');

INSERT INTO QuickPitchData.UserAccountTypes VALUES
('bios', 'Entrepreneurs');

INSERT INTO QuickPitchData.Entrepreneurs VALUES
('lildicky', '?$', 'lildicky@gmail.com', 'Lil Dicky', 'Lil Dicky',
'I have given everything away for free thus far, and spent my own money to create it all. To continue, I need your help. Please be my record label. Let me stay weird. Let me keep entertaining you. Let us change rap forever.',
'https://www.youtube.com/watch?v=QHszxmFdVrs');

INSERT INTO QuickPitchData.UserAccountTypes VALUES
('lildicky', 'Entrepreneurs');

INSERT INTO QuickPitchData.Entrepreneurs VALUES
('powerupfpv', 'C$', 'powerupfpv@gmail.com', 'Shai Goitein', 'PowerUp FPV',
'Experience first-person-view flight on a paper airplane with a LIVE streaming camera, via Google Cardboard.',
'https://www.youtube.com/watch?v=q9bpp7zmM_A');

INSERT INTO QuickPitchData.UserAccountTypes VALUES
('powerupfpv', 'Entrepreneurs');

INSERT INTO QuickPitchData.Entrepreneurs VALUES
('mycroft', '@$', 'mycroft@gmail.com', 'Joshua Montgomery', 'Mycroft',
'Uses natural language to control Internet of Things. Built on Raspberry Pi this whole home A.I. plays media, controls lights & more.',
'https://www.youtube.com/watch?v=g1G0yEKuED8');

INSERT INTO QuickPitchData.UserAccountTypes VALUES
('mycroft', 'Entrepreneurs');














