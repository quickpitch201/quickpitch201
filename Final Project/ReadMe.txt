CSCI201 Final Project - Android App - QuickPitch

In order to run the QuickPitch app, you need to have Android Studio, mySQL, and Internet access available on your computer. Please use the two provided SQL files to create the QuickPitch Database as well as some mock data entries and then turn on your SQL server. Meanwhile, please import the QuickPitchProject_v1 Android project into the Android Studio. After the project has finished opening, go to the Constants.java file found in app/java/BackEnd/Resources and change the Google API to the appropriate string if needed as well as the SQL password and your computer's current IPAddress. Then, you can run the QuickPitchServer.java file found in app/java/BackEnd/Server and build the app. For our project, we used Genymotion Google Nexus 5 - 5.00; however, you may use any simulator that works well for your computer. Once the app has finished building, it should be ready to run. Multiple clients can connect to the server at the same time and interact with one another.

Thank you for facilitating the assignment of this project. We had a great time working on it and hope that one day we can pitch it to a larger audience.

Best,

Team:
Emanuel Azage - eazage@usc.edu - 29928
Rahul Chander - chander@usc.edu - 30393			
Uddhav Joglekar - joglekar@usc.edu - 30393
Abrar Matin - amatin@usc.edu - 29928			
Srivas Tirumala - stirumal@usc.edu - 30393
Anthony Tu - tua@usc.edu - 30393

