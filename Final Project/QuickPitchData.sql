DROP DATABASE IF EXISTS QuickPitchData;
CREATE DATABASE QuickPitchData;
USE QuickPitchData;

CREATE TABLE UserAccountTypes (
	username varchar(30) NOT NULL PRIMARY KEY,
    accountType varchar(20) NOT NULL
);

CREATE TABLE Investors (
    username varchar(30) NOT NULL,
    pword varchar(100) NOT NULL,
    email varchar(50) NOT NULL,
    companyName varchar(100) NOT NULL
);

CREATE TABLE Entrepreneurs (
    username varchar(30) NOT NULL PRIMARY KEY,
    pword varchar(100) NOT NULL,
    email varchar(50) NOT NULL,
    companyName varchar(100) NOT NULL,
    companyInfo varchar(100) NOT NULL,
    productDescription varchar(2000) NOT NULL,
	videoURL varchar(200) NOT NULL
);

CREATE TABLE LikesConnections (
	connectionID INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    investor varchar(30) NOT NULL,
    entrepreneur varchar(30) NOT NULL,
    likeOrConnection varchar(20) NOT NULL
);